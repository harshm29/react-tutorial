import { useState } from 'react'
import Header from './components/Header'
import Tasks from './components/Tasks'
function App() {
  const [tasks,setTasks]=useState([
    {
        id:1,
        text:'Harsh',
        day:'March 29',
        reminder:true
    },
    {
        id:2,
        text:'Jay',
        day:'March 29',
        reminder:true
    }, {
        id:3,
        text:'Ravi',
        day:'March 29',
        reminder:true
    },
])
  return (
    <div className="container">
      <Header title={"Track System"}/>
      <Tasks tasks={tasks}/>
    </div>
  );
}

export default App;
