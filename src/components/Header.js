import PropTypes from 'prop-types'
import Button from './Button'

const Header = ({title}) => {
    const onClick =() => {
        console.log(
            'hi'
        )
    }
    return (
        <header className="header">
            <h1 > {title} </h1>
            <Button color="green" text="add" onClick={onClick}/>
        </header>
    )
}
Header.defaultProps = {
    title: 'Task for header create',
}

Header.propTypes={
    title: PropTypes.string.isRequired,
}

// const HeadingStyle ={
//     color:'white',
//     backgroundColor:'black'
// }

export default Header
